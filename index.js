const TelegramBot = require("node-telegram-bot-api");
const googleTTS = require("google-tts-api");
const FfmpegCommand = require("fluent-ffmpeg");
const { nanoid } = require("nanoid");
const fs = require("fs");

const dev = process.env.NODE_ENV !== "production";

const { TELEGRAM_TOKEN } = process.env;
if (!TELEGRAM_TOKEN) {
  throw "Sos un boludx";
}
const bot = new TelegramBot(TELEGRAM_TOKEN, { polling: true });

bot.on("message", async (msg) => {
  dev && console.log(msg);

  const chatId = msg.chat.id;

  if (!msg.text) return;

  const matches = msg.text.match(/^\/speak(@\w+)?( (en|es))?( [0-9]+)?/);
  if (!matches) return;
  dev && console.log(matches);

  const lang = matches[3] || "es";
  // speed normal = 1 (default), slow = 0.24
  const speed = matches[4] ? parseInt(matches[4]) / 100 : 1;

  if (!msg.reply_to_message) {
    return bot.sendMessage(chatId, "¡Tenés que contestarle a un mensaje!", {
      reply_to_message_id: msg.message_id,
    });
  }

  let text = msg.reply_to_message.text || msg.reply_to_message.caption;
  if (!text) {
    return bot.sendMessage(chatId, "¡Ese mensaje no tiene texto!", {
      reply_to_message_id: msg.message_id,
    });
  }

  let texts = [""];
  let characterCount = 0;
  for (const word of text.split(" ")) {
    characterCount += word.length + 1;
    if (characterCount > 200) {
      characterCount = word.length + 1;

      texts[texts.length] = word + " ";
    } else {
      texts[texts.length - 1] += word + " ";
    }
  }
  dev && console.log(texts);

  if (texts.length > 20) {
    return bot.sendMessage(chatId, "Calmate locx", {
      reply_to_message_id: msg.message_id,
    });
  }

  try {
    let urls = await Promise.all(
      texts.map((text) => googleTTS(text, lang, speed))
    );
    dev && console.log(urls);

    const fileName = "/tmp/" + nanoid();

    const ffmpeg = new FfmpegCommand();
    for (const input of urls) {
      ffmpeg.input(input);
    }
    ffmpeg
      .audioCodec("opus")
      .format("ogg")
      .on("end", async () => {
        await bot.sendVoice(
          chatId,
          fileName,
          {
            reply_to_message_id: msg.reply_to_message.message_id,
          },
          {
            contentType: "audio/ogg",
          }
        );
        await fs.promises.unlink(fileName);
        // bot.deleteMessage(chatId, msg.message_id);
      })
      .mergeToFile(fileName, "/tmp");
  } catch (error) {
    dev && console.error(error);
    await bot.sendMessage(chatId, `Hubo un problema D: ${error}`, {
      reply_to_message_id: msg.message_id,
    });
  }
});
